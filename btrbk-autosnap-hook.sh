#!/bin/bash
#
# /usr/share/libalpm/scripts/btrbk-autosnap-hook.sh
#
#   This helper script works together with an aur-helper wrapper script to ensure that only one
#   snapshot is taken per update sequence. This prevents pacman from taking two (or more) snapshots
#   during a given aur-helper update sequence.
#
#   see: https://gitlab.com/arglebargle-arch/btrbk-autosnap

# check for our flag file, if it exists then a snapshot has already been taken during this update
[[ -f "$_snap_once_" ]] && exit 0

# TODO: figure out if our snapshot target is mounted
#if ! mounted /mnt/btrfs; then
#  _snap_do_unmount_='true'
#  mount /mnt/btrfs
#fi

# Only attempt a snapshot if our config file is present
if ! [[ -f /etc/btrbk/pacman-autosnap-hook.conf ]]; then
  echo "${0##*/}: Config file /etc/btrbk/pacman-autosnap-hook.conf is missing, exiting.."
  exit 0
fi

# take the snapshot and touch our flag file to indicate that we've done so
/usr/bin/btrbk snapshot --config=/etc/btrbk/pacman-autosnap-hook.conf -q

# NOTE: Note sudo use guarded by the existence of SUDO_USER
#       this skips sudo use when root runs pacman directly
if [[ -v _snap_once_ ]]; then
  ${SUDO_USER:+sudo -u $SUDO_USER} touch "$_snap_once_"
fi

# TODO: unmount our snapshot target after
# ...

# vim:set tw=100:
