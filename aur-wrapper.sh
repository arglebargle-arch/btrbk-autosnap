#!/bin/bash
#
# aur-wrapper.sh: Prevent auto-snapshot hooks from creating multiple snapshots per update sequence
#
#   mimic paru/yay '-Syu' default behavior on no arguments
#   use powerpill if available and not using parallel pacman downloads
#   ensure only one snapshot per update sequence when using an AUR helper + filesystem snapshot hook
#
#   see: https://gitlab.com/arglebargle-arch/btrbk-autosnap

# this script works well enough with both yay and paru
aurhelper="paru"

# hooks that respect our '_snap_once_' lockfile
snapshothooks=(
  05-btrbk-autosnap.hook
  # NOTE: `timeshift-autosnap` does not know about this wrapper (yet)
  # 00-timeshift-autosnap.hook
)

exists() { for f; do type -P "$f" >&/dev/null || return 1; done; }
msg() { echo "==> ${0##*/}: $*" >&2; }
_args=()

# check for powerpill if pacman's ParallelDownloads feature isn't in use
# don't bother with powerpill if we're not syncing and/or we want sync help
if [[ ${1:--Syu} =~ ^-S.*u ]] && [[ ! $1 =~ ^-S.*h ]] &&
  exists powerpill && ! grep -Eq '^ParallelDownloads' /etc/pacman.conf; then
  msg "Using powerpill ..."
  _args+=("--pacman" "$(type -P powerpill)")
fi

# look for any of our snapshot hooks in /etc/pacman.d/hooks and /usr/share/libalpm/hooks
# if a hook is found then setup our lockfile and lock flag to restrict snapshot creation to
# the first pacman invocation only
for hook in "${snapshothooks[@]}"; do
  if  [[ -f "/etc/pacman.d/hooks/$hook" ]] ||
      [[ -f "/usr/share/libalpm/hooks/$hook" ]] ||
      # NOTE: any other paths to check go above this line
      #       'false' below allows this comment block
      false; then

    # hook found, get a path for our _snap_once_ lockfile; if defined use XDG_RUNTIME_DIR rather than /tmp
    # shellcheck disable=SC2155
    export _snap_once_="$(mktemp ${XDG_RUNTIME_DIR:+-p $XDG_RUNTIME_DIR} -ut ".snapshot-hook-XXXXXXXXXX.lock")"

    # XXX: _snap_once_ *must* be exported to pass to child shell processes

    # cleanup our lockfile post-upgrade if it exists
    # shellcheck disable=SC2064
    trap "[[ -f $_snap_once_ ]] && rm -f -- $_snap_once_" EXIT

    # allow our variable to pass through the sudo call to pacman
    _args+=("--sudoflags=--preserve-env=_snap_once_")

    # hook found & setup complete, stop searching
    break
  fi
done

# use command here to skip any shell functions or aliases and run our aur helper directly
command "$aurhelper" "${_args[@]}" "${@:--Syu}"

