
## btrbk-autosnap

This pacman hook takes btrbk snapshots automatically when pacman upgrades, installs or removes packages. When used with
the companion [aur-wrapper.sh](aur-wrapper.sh) script only the first pacman invocation of an aur helper's update process
will produce a snapshot.

Project Repo: https://gitlab.com/arglebargle-arch/btrbk-autosnap

### Installation:

Install the package and configure `/etc/btrbk/pacman-autosnap-hook.conf.example` for your system, then rename to
`/etc/btrbk/pacman-autosnap-hook.conf`

Add `alias paru=aur-wrapper.sh` or `alias yay=aur-wrapper.sh` to your `~/.bashrc`

### Debugging:

Edit `/usr/share/libalpm/hooks/05-btrbk-autosnap.hook` and edit the Exec call to include `-x` as below:

`Exec = /usr/bin/bash -x /usr/share/libalpm/scripts/btrbk-autosnap-hook.sh`

This will invoke the hook script with `bash -x` and dump all commands executed to the terminal. This should make it
possible to see what's failing.

### Uninstall:

Remove `/etc/btrbk/pacman-autosnap-hook.conf`, any aliases and any snapshots taken.

[//]: # ( vim: set tw=120: )
